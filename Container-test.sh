#!/usr/bin/env bash

# Use this file for assumptions within Gitlab CI.

set -xe

export IMAGE=registry.fedoraproject.org/fedora-minimal@sha256:5a6ca4d2b2923ab84f10601b3b1d5c6246586de8b0bdbf1b94dd59de473fb68a

ctr1=$(buildah from --arch "$ARCH" $IMAGE)

 # renovate: datasource=github-tags depName=containers/buildah
export BUILDAH_VERSION=v1.21.1


buildah add "$ctr1" https://raw.githubusercontent.com/containers/buildah/$BUILDAH_VERSION/contrib/buildahimage/stable/containers.conf /etc/containers/

buildah run "$ctr1" bash -c "useradd build \
&& microdnf -y update \
&& microdnf -y reinstall shadow-utils \
&& microdnf -y install buildah podman fuse-overlayfs xz \
&& chmod 644 /etc/containers/containers.conf \
&& sed -i -e 's|^#mount_program|mount_program|g' -e '/additionalimage.*/a \"/var/lib/shared\x22,' -e 's|^mountopt[[:space:]]*=.*$|mountopt = \x22nodev,fsync=0\x22|g' /etc/containers/storage.conf \
&& mkdir -p /var/lib/shared/overlay-images /var/lib/shared/overlay-layers /var/lib/shared/vfs-images /var/lib/shared/vfs-layers \
&& touch /var/lib/shared/overlay-images/images.lock \
&& touch /var/lib/shared/overlay-layers/layers.lock \
&& touch /var/lib/shared/vfs-images/images.lock \
&& touch /var/lib/shared/vfs-layers/layers.lock \
&& echo build:2000:50000 > /etc/subuid \
&& cat /etc/containers/storage.conf \
&& echo build:2000:50000 > /etc/subgid"

buildah config --env BUILDAH_ISOLATION=chroot "$ctr1"
buildah config --author='Kutara Cloud <hello@anthonyrabbito.com>' "$ctr1"
buildah config --volume='/var/lib/containers' --volume='/home/build/.local/share/containers' "$ctr1"
echo $BUILDAH_VERSION > build/upstream-version
buildah rm "$ctr1"